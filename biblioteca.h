#ifndef BIBLIOTECA_H
#define BIBLIOTECA_H

#include <iostream>

void mostrarMenuInicio();
void mostrarInstrucciones();
void mostrarCreditos();

void moverJugador();
void atacarEnemigo();
void gestionarColisiones();
void generarMapa();
void generarLlaves();
void gestionarVidas();
void gestionarTiempo();
void gestionarDialogos();
void gestionarTurnosPelea();
void opcionesJugador();
void mostrarBarraProgreso();
void gestionarPuntaje();
void gestionarNiveles();
void mostrarPantallaGanar();
void mostrarPantallaPerder();

#endif