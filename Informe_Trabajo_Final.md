# Repositorio del Juego: https://gitlab.com/SlyceCode/hide-game

# Informe Preliminar del Trabajo Final (TF)

# **1. Introducción ✍🏻**

### Situacion problemática:

**Contexto:** El problema encontrado está relacionado con los estudiantes de quinto y sexto grado de primaria que enfrentan dificultades para comprender conceptos matemáticos específicos como los **_números positivos y negativos_** y las **_ecuaciones con 1 incógnita._**

**Motivación:** Ayudar a mejorar al aprendizaje de los estudiantes que genere una experiencia agradable de forma didáctica y única para ellos. A la vez que aplicamos y mejores nuestros conocimientos sobre los temas del curso.

### Propuesta de Solución:

Crear un videojuego en C++ con mecánicas atractivas y entretenidas que capturen el interés del usuario facilitando el aprendizaje de conceptos matemáticos, a través de las mecánicas del juego.

### **Objetivo:**

Implementar un videojuego modo historia en la consola utilizando el lenguaje C++ sobre números positivos y negativos además de ecuaciones con 1 incógnita para que de esa manera los estudiantes de quinto y sexto grado de primaria logren un mejor aprendizaje y rendimiento académico.

# **2. Desarrollo**

| Tipo de Personajes  | Cantidad |
| :------------------ | :------: |
| Personaje Principal |    1     |
| Enemigos            |    4     |
| Tutores             |    4     |

### Funcionalidades de los personajes:

**Personaje Principal:** Pasará distintos niveles para derrotar a cada enemigo, cada enemigo tiene una llave, el cual el personaje principal deberá derrotalos resolviendo distintos puzzles donde utilizará conceptos matemáticos para completarlos, al conseguir todas las llaves conseguirá escapar de una cueva en la que estaba atrapado.

**Enemigos:** Crean los puzzles y desafios a resolver, evitando que sumemos puntos y obtengamos la llave, para luego tener una batalla final con ellos.

**Tutores:** Nos ayudan y nos dan pistas para resolver los puzzles, además de brindarnos vidas extras.

# **3. Conclusiones:**

Se obtuvo el logro de haber realizado un videojuego según nuestra planificación en donde ayudamos a los niños de 5to y 6to grado de primaria a que puedan identificar y resolver problemas con números positivos y negativos y a su vez, ecuaciones con una incógnita.

# **4. Requisitos Funcionales**

| RID  | Requisito para el Juego                                                 |
| ---- | ----------------------------------------------------------------------- |
| R001 | Implementar un menú de Inicio, Instrucciones y Créditos                 |
| R002 | Implementar el movimiento del Jugador                                   |
| R003 | Implementar los ataques del Enemigo                                     |
| R004 | Implementar un sistema de colisiones                                    |
| R005 | Implementar un Generador de Mapas                                       |
| R006 | Implementar un generador de llaves                                      |
| R007 | Implementar un sistema de Vidas                                         |
| R008 | Implementar un sistema de tiempo                                        |
| R009 | Implementar un sistema de Diálogos                                      |
| R010 | Implementar un sistema de Turnos de Pelea                               |
| R011 | Implementar un sistema de opciones para el Jugador (Atacar, Defenderse) |
| R012 | Implementar una barra de progreso para el estudiante                    |
| R013 | Implementar un sistema de puntaje                                       |
| R014 | Implementar un sistema de niveles                                       |
| R015 | Implementar una pantalla de Ganar y Perder                              |

# **5. Diagrama de Modulos**

https://ibb.co/CmCND0Y

# **5.1 Protipo Mapa**

https://ibb.co/NW5f0kF

# **6. Dividir el Trabajo.**

| Estudiante        |               Tarea                |
| :---------------- | :--------------------------------: |
| Antonio Ropón     | R002, R003, R004, R009, R011, R014 |
| Fabrizio Chapoñan |       R001, R006, R008, R013       |
| Carlos Cespedes   |    R005, R007, R010, R012, R015    |
